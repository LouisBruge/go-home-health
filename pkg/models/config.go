package models

type (
	// Config represents main application configuration
	Config struct {
		Scopes         []string             `json:"scopes" yaml:"scopes" env:"SCOPES" required:"true"`
		Authentication AuthenticationConfig `json:"authentication" yaml:"authentication"`
	}

	// AuthenticationConfig represents configuration for Authentication to Netatmo API
	AuthenticationConfig struct {
		Password     string `json:"password" yaml:"password" env:"AUTH_PASSWORD" required:"true"`
		User         string `json:"user" yaml:"user" env:"AUTH_USER" required:"true"`
		ClientID     string `json:"clientID" yaml:"client_id" env:"AUTH_CLIENT_ID" required:"true"`
		ClientSecret string `json:"clientSecret" yaml:"client_secret" env:"AUTH_CLIENT_SECRET" required:"true"`
	}
)
