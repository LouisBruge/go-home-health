# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.1.0] - 2021-05-21
### Added
- Method to retrieve Netatmo Home Coach data with context
- Option to override default HTTP Client

## [v0.0.1] - 2021-05-03
### Added
- HTTP client to retrieve Netatmo Home Coach data
- Simple client example

[Unreleased]: https://gitlab.com/LouisBruge/go-home-health/-/compare/master...v0.1.0
[v0.1.0]: https://gitlab.com/LouisBruge/go-home-health/-/compare/v0.0.1...v0.1.0
[v0.0.1]: https://gitlab.com/LouisBruge/go-home-health/-/tags/v0.0.1
