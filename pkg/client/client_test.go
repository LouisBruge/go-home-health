package client_test

import (
	"context"
	"io"
	"net/http"
	"os"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/ginkgo/extensions/table"
	. "github.com/onsi/gomega"
	"github.com/pkg/errors"

	"github.com/stretchr/testify/mock"
	"gitlab.com/LouisBruge/go-home-health/pkg/client"
)

// DoerMock is a mock implementation of client.HTTPDoer interface using underlying mock.Mock instance
type DoerMock struct {
	mock.Mock
}

// Do is a mock method Do that realises http calls
func (d *DoerMock) Do(r *http.Request) (*http.Response, error) {
	args := d.Called(r)
	return args.Get(0).(*http.Response), args.Error(1)
}

func mustOpenFile(filename string) io.Reader {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	return file
}

var _ = Describe("Client", func() {
	Describe("GetHomeCoachDevicesWithContext()", func() {
		type errorCase struct {
			context    string
			statusCode int
			response   *http.Response
			err        error
		}
		DescribeTable("Error while authenticate", func(c errorCase) {
			Context(c.context, func() {
				doer := &DoerMock{}

				doer.
					On("Do", mock.Anything).
					Return(c.response, c.err).
					Once()

				devices, err := client.NewHTTPClient(client.OptionWithClient(doer)).GetHomeCoachDevicesWithContext(context.TODO())

				Expect(err).NotTo(BeNil())
				Expect(devices).To(BeEmpty())
			})
		},
			Entry("on bad request status code", errorCase{
				context:    "Bad request",
				statusCode: http.StatusBadRequest,
				response: &http.Response{
					Body: io.NopCloser(strings.NewReader(`{
							"error": "invalid_request",
							"error_description": "Missing parameters, "username" and "password" are required",
						}`)),
					StatusCode: http.StatusBadRequest,
				},
				err: nil,
			}),
			Entry("on error response with invalid body", errorCase{
				context:    "Should return an error on invalid body on error even if the status code OK",
				statusCode: http.StatusInternalServerError,
				response: &http.Response{
					Body:       io.NopCloser(strings.NewReader(`foo`)),
					StatusCode: http.StatusOK,
				},
				err: nil,
			}),
			Entry("on error response with invalid body", errorCase{
				context:    "Should return an error on invalid body on error",
				statusCode: http.StatusInternalServerError,
				response: &http.Response{
					Body:       io.NopCloser(strings.NewReader(`foo`)),
					StatusCode: http.StatusInternalServerError,
				},
				err: nil,
			}),
			Entry("on unexpected error", errorCase{
				context: "unexpected error on authenticate",
				err:     errors.New("unexpected error"),
			}),
		)

		Context("Successful OAuth and Entity response", func() {
			It("Should return a slice of devices", func() {
				doer := &DoerMock{}

				doer.On("Do", mock.Anything).
					Return(
						&http.Response{
							StatusCode: http.StatusOK,
							Body:       io.NopCloser(strings.NewReader("{\"access_token\":\"2YotnFZFEjr1zCsicMWpAA\",\"expires_in\":10800,\"refresh_token\":\"tGzv3JOkF0XG5Qx2TlKWIA\"}")),
						},
						nil,
					).
					Once()

				doer.On("Do", mock.Anything).
					Return(
						&http.Response{
							StatusCode: http.StatusOK,
							Body:       io.NopCloser(mustOpenFile("./testdata/valid_home_coach_data_body.json")),
						},
						nil,
					).
					Once()

				c := client.NewHTTPClient(
					client.OptionWithClient(doer),
				)

				devices, err := c.GetHomeCoachDevicesWithContext(context.TODO())

				Expect(err).To(BeNil())
				Expect(devices).To(HaveLen(1))

				doer.AssertExpectations(GinkgoT())
			})
		})
	})
})
