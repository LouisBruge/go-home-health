# Go Home Health _(go-home-health)_

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)
[![Go Report
Card](https://goreportcard.com/badge/gitlab.com/LouisBruge/go-home-health)](https://goreportcard.com/report/gitlab.com/LouisBruge/go-home-health)
[![pipeline status](https://gitlab.com/LouisBruge/go-home-health/badges/master/pipeline.svg)](https://gitlab.com/LouisBruge/go-home-health/-/commits/master)
[![coverage report](https://gitlab.com/LouisBruge/go-home-health/badges/master/coverage.svg)](https://gitlab.com/LouisBruge/go-home-health/-/commits/master)

Go Home Health is a Golang library to retrieve home-coach data exported by Netatmo API. This project has **not affiliation with Netatmo**

## Install
Before you begin, ensure you have met the following requirements:
| Requirement | Minimal Version |
| ---: | :--- |
| Go | v1.16 |

Use *go* package manager to install Healthcare library.
```sh
go get gitlab.com/LouisBruge/healthcare
```

## Usage
Before you begin, ensure you have create an account on [Netatmo Dev platform](https://dev.netatmo.com/) and create an [application](https://dev.netatmo.com/apps/createanapp#form). 

```go
package main

import (
	"gitlab.com/LouisBruge/go-home-health/models"
	"gitlab.com/LouisBruge/go-home-health/pkg/client"
)
func main() {
    client := client.NewHTTPClient(
		client.OptionWithScopes("read_homecoach"),
		client.OptionWithPassword("your_netatmo_password"),
		client.OptionWithUser("your_netatmo_user"),
		client.OptionWithClientID("your_client_id"),
		client.OptionWithClientSecret("your_client_secret"),
	)

	devices, err := client.GetHomeCoachDevices()
	if err != nil {
		log.Fatal(err)
	}

	device := devices.GetByName("home-coach")
}
```
For more details, see [/examples](examples) folder.

## Contributing
This project isn't open to contribution.

## License
This project is under MIT License.
See license in [MIT](LICENSE).
