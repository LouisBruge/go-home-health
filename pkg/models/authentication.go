package models

import (
	"encoding/json"
	"fmt"
	"time"
)

type oauthResponseDTO struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int64  `json:"expires_in"`
}

// OAuthResponseDTO represents OAuth Response Data Transfer Object
type OAuthResponseDTO struct {
	oauthResponseDTO
	emitAtUnix int64 `json:"-"`
}

// ExpireAt returns date of access token's expiration
func (o *OAuthResponseDTO) ExpireAt() time.Time {
	return o.EmitAt().Add(time.Second * time.Duration(o.ExpiresIn))
}

// EmitAt returns date when the acces token was issued
func (o *OAuthResponseDTO) EmitAt() time.Time {
	return time.Unix(o.emitAtUnix, 0)
}

// IsExpired returns true if access is expired and need to be renew
func (o *OAuthResponseDTO) IsExpired() bool {
	return o.ExpireAt().Before(time.Now())
}

// UnmarshalJSON unmarshal OAuthResposneDTO and set expireAt field
func (o *OAuthResponseDTO) UnmarshalJSON(b []byte) error {
	var oauth oauthResponseDTO
	if err := json.Unmarshal(b, &oauth); err != nil {
		return err
	}

	o.oauthResponseDTO = oauth
	o.emitAtUnix = time.Now().Unix()
	return nil
}

// OAuthErrorResponseDTO represents OAuth Response Data Transfer Object on error case
type OAuthErrorResponseDTO struct {
	Error       string `json:"error"`
	Description string `json:"error_description"`
}

// String return formated string representation of OAuthErrorResponseDTO
func (o OAuthErrorResponseDTO) String() string {
	return fmt.Sprintf("%s: %s", o.Error, o.Description)
}
