package client

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/LouisBruge/go-home-health/pkg/models"
)

// Auth url params
const (
	GrantType    = "grant_type"
	ClientID     = "client_id"
	ClientSecret = "client_secret"
	Username     = "username"
	Password     = "password"
	Scope        = "scope"
)

// Headers
const (
	ContentTypeHeader = "Content-Type"

	AcceptHeader = "Accept"

	AuthorisationHeader = "Authorization"
)

// Mine Types
const (
	FormUrlencodedMimeType string = "application/x-www-form-urlencoded;charset=UTF-8"

	JSONMimeType string = "application/json"
)

const (
	// GrantTypePassword grant type of type password
	GrantTypePassword = "password"
)

const (
	// OAuth2Endpoint endpoint to request oauth token
	OAuth2Endpoint = "https://api.netatmo.com/oauth2/token"

	// HomeCoachEndpoint endpoint to retrive Home Coach Devices data
	HomeCoachEndpoint = "https://api.netatmo.com/api/gethomecoachsdata"
)

const defaultTimeout time.Duration = time.Second * 30

// HTTPDoer defines interface of a client in charge to handle http request
type HTTPDoer interface {
	Do(r *http.Request) (*http.Response, error)
}

type (
	HTTPClient struct {
		mu           sync.Mutex
		timeout      time.Duration
		password     string
		user         string
		clientId     string
		clientSecret string
		scopes       []string
		oauthTokens  *models.OAuthResponseDTO
		client       HTTPDoer
	}
)

func NewHTTPClient(opts ...Option) *HTTPClient {
	c := HTTPClient{
		timeout: defaultTimeout,
	}

	for _, opt := range opts {
		opt(&c)
	}

	if c.client == nil {
		c.client = &http.Client{
			Timeout: c.timeout,
		}
	}
	return &c
}

// GetHomeCoachDevicesWithContext queries Home Coach Devices with context
func (h *HTTPClient) GetHomeCoachDevicesWithContext(ctx context.Context) (models.Devices, error) {
	return h.getHomeCoachDevices(context.Background())
}

// GetHomeCoachDevices queries Home Coach Devices
func (h *HTTPClient) GetHomeCoachDevices() (models.Devices, error) {
	return h.getHomeCoachDevices(context.Background())
}

// getHomeCoachDevices retrieve Home Coach Devices
func (h *HTTPClient) getHomeCoachDevices(ctx context.Context) (models.Devices, error) {
	accessToken, err := h.GetAccessToken()
	if err != nil {
		return nil, errors.Wrap(err, "error on retrieving access token")
	}

	req, err := http.NewRequest(http.MethodGet, HomeCoachEndpoint, http.NoBody)
	if err != nil {
		return nil, errors.Wrap(err, "error on building request to retrive home coach devices")
	}

	req.WithContext(ctx)

	req.Header.Add(AuthorisationHeader, fmt.Sprintf("Bearer %s", accessToken))
	req.Header.Add(AcceptHeader, JSONMimeType)

	resp, err := h.client.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "error on querying home coach devices")
	}
	log.Printf("%s %s [%d] %s", http.MethodGet, HomeCoachEndpoint, resp.StatusCode, resp.Status)
	defer resp.Body.Close()

	var payload models.ResponseDTO
	if e := json.NewDecoder(resp.Body).Decode(&payload); e != nil {
		return nil, errors.Wrap(e, "error on parsing home coach body response")
	}
	return payload.Body.Devices, nil
}

// GetAccessToken returns access token if it has not expired otherwise request a new token before returning it
func (h *HTTPClient) GetAccessToken() (string, error) {
	if h.oauthTokens != nil && !h.oauthTokens.IsExpired() {
		log.Printf("access token still valid. Used it! : %t", h.oauthTokens.ExpireAt().Before(time.Now()))
		return h.oauthTokens.AccessToken, nil
	}

	oauthPayload, err := h.Authenticate()
	if err != nil {
		return "", err
	}
	h.mu.Lock()
	defer h.mu.Unlock()

	h.oauthTokens = oauthPayload
	return h.oauthTokens.AccessToken, nil
}

// Authenticate realizes authenticate request of type grant type password and returns the response body
func (h *HTTPClient) Authenticate() (*models.OAuthResponseDTO, error) {
	data := url.Values{}
	data.Set(GrantType, GrantTypePassword)
	data.Set(Password, h.password)
	data.Set(Username, h.user)
	data.Set(ClientID, h.clientId)
	data.Set(ClientSecret, h.clientSecret)
	data.Set(Scope, strings.Join(h.scopes, " "))

	buf := data.Encode()

	req, err := http.NewRequest(http.MethodPost, OAuth2Endpoint, strings.NewReader(buf))
	if err != nil {
		return nil, errors.Wrap(err, "unexpected error on building authentication request")
	}

	req.Header.Add(ContentTypeHeader, FormUrlencodedMimeType)

	resp, err := h.client.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "unexpected error on sending authentication request")
	}
	defer resp.Body.Close()

	log.Printf("%s %s [%d] %s", http.MethodPost, OAuth2Endpoint, resp.StatusCode, resp.Status)

	if resp.StatusCode >= 400 {
		var errPayload models.OAuthErrorResponseDTO
		if e := json.NewDecoder(resp.Body).Decode(&errPayload); e != nil {
			return nil, errors.New(fmt.Sprintf("request failed with stataus %d: %s", resp.StatusCode, errors.Wrap(e, "unexpected error on decoding error response body")))
		}
		return nil, errors.New(fmt.Sprintf("request failed with status %d: %s", resp.StatusCode, errPayload))
	}
	var oauth models.OAuthResponseDTO

	if e := json.NewDecoder(resp.Body).Decode(&oauth); e != nil {
		return nil, errors.Wrap(e, "unexpected error on decode authentication body payload")
	}
	return &oauth, nil
}
