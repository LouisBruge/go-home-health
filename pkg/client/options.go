package client

// Option is utility type for functions that allow to pass option to HTTPClient
type Option func(h *HTTPClient)

// OptionWithPassword allow to set authentication password
func OptionWithPassword(p string) Option {
	return func(h *HTTPClient) {
		h.password = p
	}
}

// OptionWithUser allow to set authentication user
func OptionWithUser(u string) Option {
	return func(h *HTTPClient) {
		h.user = u
	}
}

// OptionWithClientID allow to set Client ID
func OptionWithClientID(clientID string) Option {
	return func(h *HTTPClient) {
		h.clientId = clientID
	}
}

// OptionWithScopes allows to set scopes
func OptionWithScopes(scopes ...string) Option {
	return func(h *HTTPClient) {
		h.scopes = append(h.scopes, scopes...)
	}
}

// OptionWithClientSecret allow to set Client Secret
func OptionWithClientSecret(clientSecret string) Option {
	return func(h *HTTPClient) {
		h.clientSecret = clientSecret
	}
}

// OptionWithClient allows to set a HTTP client
func OptionWithClient(client HTTPDoer) Option {
	return func(h *HTTPClient) {
		h.client = client
	}
}
