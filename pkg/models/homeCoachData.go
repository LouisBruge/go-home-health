package models

// Devices represents a slice of devices
type Devices []DeviceDTO

// GetByName retrieve a device given its name
func (d Devices) GetByName(name string) *DeviceDTO {
	if len(d) == 0 {
		return nil
	}

	for _, device := range d {
		if device.StationName == name {
			return &device
		}
	}

	return nil
}

// ResponseDTO represents Response Data Transfert Object
type ResponseDTO struct {
	Status    string  `json:"stataus"`
	TimeExec  float64 `json:"time_exec"`
	Timestamp int64   `json:"time_server"`
	Body      struct {
		User    UserDTO `json:"user"`
		Devices Devices `json:"devices"`
	} `json:"body"`
}

// DeviceDTO represent Device Data Transfert Object
type DeviceDTO struct {
	ID              string           `json:"_id"`
	StationName     string           `json:"station_name"`
	DateSetup       int              `json:"date_setup"`
	LastSetup       int              `json:"last_setup"`
	Type            string           `json:"type"`
	LastStatusStore int              `json:"last_status_store"`
	ModuleName      string           `json:"module_name"`
	Firmware        int              `json:"firmware"`
	WifiStatus      int              `json:"wifi_status"`
	Reachable       bool             `json:"reachable"`
	CO2Calibrating  bool             `json:"co2_calibrating"`
	DataType        []string         `json:"data_type"`
	Place           DevicePlaceDTO   `json:"place"`
	Data            HomeCoachDataDTO `json:"dashboard_data"`
}

// DevicePlaceDTO represent Device's place Data Transfert Object
type DevicePlaceDTO struct {
	Altitude float32   `json:"altitude"`
	City     string    `json:"city"`
	Country  string    `json:"country"`
	Timezone string    `json:"timezone"`
	Location []float32 `json:"location"`
}

// HomeCoachDataDTO represent Home Coach Data Transfert Object
type HomeCoachDataDTO struct {
	Timestamp        int     `json:"time_utc"`
	Temperature      float32 `json:"Temperature"`
	CO2              float32 `json:"CO2"`
	Humidity         float32 `json:"Humidity"`
	Noise            float32 `json:"Noise"`
	Pressure         float32 `json:"Pressure"`
	AbsolutePressure float32 `json:"AbsolutePressure"`
	HealthIDX        uint    `json:"health_idx"`
	MinTemp          float32 `json:"min_temp"`
	MaxTemp          float32 `json:"max_temp"`
	TimestampMaxTemp int     `json:"date_max_temp"`
	TimestampMinTemp int     `json:"date_min_temp"`
}

// UserDTO represent the User Data Transfert Objet
type UserDTO struct {
	Mail           string `json:"mail"`
	Administrative struct {
		Lang         string `json:"lang"`
		Locale       string `json:"reg_locale"`
		Country      string `json:"country"`
		Unit         int    `json:"unit"`
		WindUnit     int    `json:"windunit"`
		PressureUnit int    `json:"pressureunit"`
		FeelLikeAlgo int    `json:"feel_like_algo"`
	}
}
