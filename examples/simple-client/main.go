package main

import (
	"log"
	"time"

	"github.com/jinzhu/configor"
	"gitlab.com/LouisBruge/go-home-health/pkg/models"
	"gitlab.com/LouisBruge/go-home-health/pkg/client"
)

func main() {
	var config models.Config

	if e := configor.
		New(&configor.Config{AutoReload: true, AutoReloadInterval: time.Minute * 1}).
		Load(&config, "config.yaml"); e != nil {
		log.Fatalf("error on loading configuration file: %v", e.Error())
	}

	client := client.NewHTTPClient(
		client.OptionWithScopes(config.Scopes...),
		client.OptionWithPassword(config.Authentication.Password),
		client.OptionWithUser(config.Authentication.User),
		client.OptionWithClientID(config.Authentication.ClientID),
		client.OptionWithClientSecret(config.Authentication.ClientSecret),
	)

	devices, err := client.GetHomeCoachDevices()
	if err != nil {
		log.Fatal(err)
	}

	device := devices.GetByName("bedroom")

	log.Printf("device %#v", *device)
}
