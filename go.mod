module gitlab.com/LouisBruge/go-home-health

go 1.16

require (
	github.com/jinzhu/configor v1.2.1
	github.com/onsi/ginkgo v1.16.2
	github.com/onsi/gomega v1.12.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0
)
